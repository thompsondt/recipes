---
title: White Asparagus Soup
created: 2024-04-04T03:22:34
---

White asparagus soup is a creamy, flavorful dish that’s especially popular in the spring when white asparagus is in season. This soup pairs beautifully with a crusty bread or a light salad. 

Ingredients:
- 500g white asparagus
- 1 large onion, finely chopped
- 1 litre vegetable or chicken broth
- 100ml cream
- 2 tablespoons butter
- Salt and white pepper, to taste
- Nutmeg, a pinch (optional)
- Fresh parsley or chervil, finely chopped (for garnish)

Instructions:
1. Prepare the Asparagus:
    - Peel the white asparagus thoroughly to remove the tough outer skin.
    - Cut off the woody ends.
    - Chop the asparagus into small pieces, setting the heads aside.

2. Cook the Onion:
	In a large pot, melt the butter over medium heat.
    Add the chopped onion and sauté until it’s translucent, about 5 minutes.

3. Add Asparagus:
	- Add the chopped asparagus stems to the pot (excluding the heads) and sauté for a few minutes. Then, pour in the broth and bring to a boil.
    - Lower the heat and let it simmer until the asparagus is very tender, about 15-20 minutes.

4. Blend the Soup:
	- Once the asparagus is soft, use an immersion blender to purée the soup until smooth. If you prefer a very fine texture, you can strain the soup through a sieve.

5. Cook Asparagus Heads:
	- Add the asparagus heads to the soup and simmer for an additional 5-10 minutes, or until they are tender but still slightly crisp.

6. Final Seasoning:
	- Stir in the cream and bring the soup back up to a gentle simmer. ===Do not boil.===
    - Season with salt, white pepper, and a pinch of nutmeg to taste.

7. Serve:
    - Ladle the soup into bowls and garnish with fresh parsley or chervil. Serve hot.