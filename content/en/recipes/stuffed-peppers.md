---
title: Healthier Stuffed Peppers
---
Time 2hrs

#### Ingredients

---
Filling Part 1: Rice (45 - 50 minutes to prep for Pepper Filling)
- 1 cup water
- ½ cup brown rice
---
Filling Part 2: Meat Mix (Mixed in a Large Bowl and Cooked for 5 minutes in a hot skillet)
- 1 pound lean ground beef
- 1 onion, chopped
- 2 cloves garlic, minced
---
Peppers: Prep Stage (tops and hearts removed)
- 2 green bell peppers
- 2 red bell peppers
- 2 yellow bell peppers
---
Filling Part 2A: (Added to the meat mix after browning)
- 1 (8 ounce) cans natural tomato sauce
- 1 tablespoon Worcestershire sauce
- salt and ground black pepper to taste
---
Sauce (Mixed and later poured over peppers): 
- 1 (8 oz) can of natural tomato sauce
- 1 teaspoon Italian seasoning
---
Final Dressing:
- ¼ cup grated Parmesan cheese, optional
---

#### Instructions

1. Filling Part 1 (Rice)  - Bring water and brown rice to a boil in a saucepan. Reduce heat to medium-low, cover, and simmer until rice is tender and liquid is absorbed, 45 to 50 minutes.
2. Filling Part 2 (Meat Mix) - In a large mixing bowl, combine beef, onion, and garlic. Add to a hot skillet; cook and stir until meat is evenly browned and onion is softened, about 5 minutes. Return to the mixing bowl.
3. Pepper Prep - Remove and discard tops, seeds, and membranes of green, red, and yellow bell peppers. Arrange peppers in a baking dish with the hollowed sides facing upward. Slice the bottoms off peppers if necessary so that they stand upright.
4. Filling Part 2A - Add the rice, tomato sauce, Worcestershire sauce, salt, and black pepper to the Meat Mix
5. Preheat the oven to 350 degrees F (175 degrees C).
6. Pour the sauce mix over the peppers
7. Bake in the preheated oven, basting with sauce every 15 minutes, until peppers are tender, about 1 hour. Sprinkle peppers with grated Parmesan cheese; serve warm.